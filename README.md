## Readme - MCS Backend 

Getting Started
=================
**Requirements**
  - docker *ver. 17.06.0-ce*
  - docker-compose *ver. 1.14.0*

1. Clone this Repository
2. Run:
  - `cd mcs-be`
  - `docker-compose build`
  - `make install`
  - `docker-compose up` or `docker-compose up -d` (demonize)
  - to follow logs when demonized `docker-compose logs -f`

(notice: *`make install` is only required for your* ***first installation*** *or after installation of a* ***new npm package***)
