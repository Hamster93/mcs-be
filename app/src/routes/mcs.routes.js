import emergencyRoutes from './mcs.emergency.routes.js'
import locationRoutes from './mcs.location.routes.js'
import officerRoutes from './mcs.officer.routes.js'
import unitRoutes from './mcs.unit.routes.js'
import AuthController from '../controllers/auth.controller'
import TokenCheck from '../connections/authenticateToken'
const tokenCheck = new TokenCheck();
const authController = new AuthController()

export default (server) => {

  server.post('/mcs/auth/', (req, res) => {
    authController.doAuthentication(req.body.pseudonym, req.body.pw)
    .then((result) => res.json(result))
  })

  officerRoutes(server)
  unitRoutes(server)
  emergencyRoutes(server)
  locationRoutes(server)
}
