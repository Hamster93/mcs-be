import LocationController from '../controllers/location.controller'
const locationController = new LocationController()

export default (server) => {
  server
  .get('/mcs/location/:id', (req, res) => {
    locationController.getLocation(req.params.id)
    .then( location => res.send(location) )
    .catch( err => res.send(err) )
  })
  .get('/mcs/location/', (req, res) => {
    locationController.getLocations()
    .then( locations => res.send(locations) )
    .catch( err => res.send(err) )
  })
  .post('/mcs/location/', (req, res) => {
    locationController.createLocation(req.body)
    .then( () => res.send('Saved') )
    .catch( err => res.send(err) )
  })
  .put('/mcs/location/', (req, res) => {
    locationController.updateLocation(req.body)
    .then( () => res.send('Updated') )
    .catch( err => res.send(err) )
  })
}
