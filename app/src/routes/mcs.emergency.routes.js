import EmergencyController from '../controllers/emergency.controller'
const emergencyController = new EmergencyController()

export default (server) => {
  server
  .get('/mcs/emergency/:id', (req, res) => {
    emergencyController.getEmergency(req.params.id)
    .then( emergency => res.send(emergency) )
    .catch( err => res.send(err) )
  })
  .get('/mcs/emergency/', (req, res) => {
    emergencyController.getEmergencies()
    .then( emergencies => {
      res.send(emergencies)
    })
    .catch( err => res.send(err) )
  })
  .get('/mcs/unitovertakenemergency/:unitId', (req, res) => {
    emergencyController.getOvertakenEmergency(req.params.unitId)
    .then( emergency => {
      console.log('overtaken em in ROUTES: ' + emergency);
      res.send(emergency)
    })
    .catch( err => res.send(err) )
  })
  .post('/mcs/emergency/', (req, res) => {
    emergencyController.createEmergency(req.body)
    .then( () => res.send('Saved') )
    .catch( err => res.send(err) )
  })
  .put('/mcs/emergency/', (req, res) => {
    emergencyController.updateEmergency(req.body)
    .then( () => res.send('Updated') )
    .catch( err => res.send(err) )
  })
}
