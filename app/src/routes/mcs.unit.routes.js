import UnitController from '../controllers/unit.controller'
const unitController = new UnitController()

export default (server) => {
  server
  .get('/mcs/unit/:id', (req, res) => {
    unitController.getUnit(req.params.id)
    .then( unit => res.send(unit) )
    .catch( err => res.send(err) )
  })
  .get('/mcs/unitofficer/:officerId', (req, res) => {
    unitController.getUnitByOfficer(req.params.officerId)
    .then( units => res.send(units) )
    .catch( err => res.send(err) )
  })
  .get('/mcs/unit/', (req, res) => {
    unitController.getUnits()
    .then( units => res.send(units) )
    .catch( err => res.send(err) )
  })
  .post('/mcs/unit/', (req, res) => {
    unitController.createUnit(req.body)
    .then( () => res.send('Saved') )
    .catch( err => res.send(err) )
  })
  .put('/mcs/unit/', (req, res) => {
    unitController.updateUnit(req.body)
    .then( () => res.send('Updated') )
    .catch( err => res.send(err) )
  })
}
