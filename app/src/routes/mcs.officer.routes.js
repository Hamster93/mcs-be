import OfficerController from '../controllers/officer.controller'
const officerController = new OfficerController()

export default (server) => {
  server
  .get('/mcs/officer/:id', (req, res) => {
    officerController.getOfficer(req.params.id)
    .then( officer => res.send(officer) )
    .catch( err => res.send(err) )
  })
  .get('/mcs/officer/', (req, res) => {
    officerController.getOfficers()
    .then( officers => res.send(officers) )
    .catch( err => res.send(err) )
  })
  .post('/mcs/officer/', (req, res) => {
    officerController.createOfficer(req.body)
    .then( () => res.send('Saved') )
    .catch( err => res.send(err) )
  })
  .put('/mcs/officer/', (req, res) => {
    officerController.updateOfficer(req.body)
    .then( () => res.send('Updated') )
    .catch( err => res.send(err) )
  })
}

// passport.authenticate('jwt', { session: false })


/*
Cop 13 - root
"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfYnNvbnR5cGUiOiJPYmplY3RJRCIsImlkIjp7InR5cGUiOiJCdWZmZXIiLCJkYXRhIjpbOTAsNDksMTc1LDE2OCwyMDAsNzIsMywxLDE0MywxMTAsMTIsMTc0XX0sImlhdCI6MTUxMzI1MzcwNn0.bH1EMoT6m-MH8Z5L7fye5imgs-fMSQXiwqBMFImDh7I"
*/
