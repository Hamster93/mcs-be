import { mongoose } from '../connections/mongodb'
import bcrypt from 'bcrypt-nodejs'

var Schema = mongoose.Schema
var officerSchema = new Schema({
    name: String,
    pseudonym: { type: String, unique: true, required: true },
    pw: { type: String, unique: false, required: true },
    status: Number,
    unit: { type: Schema.ObjectId, ref: 'Unit' }
}, { collection: 'officers' })

officerSchema.pre('save', function (next) {
  var user = this
  if (this.isModified('pw') || this.isNew) {
    bcrypt.genSalt(10, function (err, salt) {
      if (err) {
        return next(err)
      }
      bcrypt.hash(user.pw, salt, null, function(err, hash) {
        if (err) {
          return next(err)
        }
        user.pw = hash
        next()
      })
    })
  } else {
    return next()
  }
})

console.log('+++Officer schema created')
module.exports = mongoose.model('Officer', officerSchema)
