import { mongoose } from '../connections/mongodb'

var Schema = mongoose.Schema;
var locationSchema = new Schema({
    creationDate: String,
    longtitude: String,
    latitude: String,
    adress: String,
    description: String
}, { collection: 'locations' })

console.log('+++Location schema created')
export default mongoose.model('Location', locationSchema)
