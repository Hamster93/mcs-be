import { mongoose } from '../connections/mongodb'

var Schema = mongoose.Schema;
var unitSchema = new Schema({
    name: String,
    type: String,
    capacity: String,
    equipment: String,
    status: Number,
    location: {
      creationDate: String,
      longtitude: String,
      latitude: String,
      adress: String,
      description: String
    },
    officer: [ { type: Schema.ObjectId, ref: 'Officer' } ]
}, { collection: 'units' });

console.log('+++Unit schema created')
export default mongoose.model('Unit', unitSchema)
