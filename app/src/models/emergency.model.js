import { mongoose } from '../connections/mongodb'

var Schema = mongoose.Schema;
var emergencySchema = new Schema({
  creationDate: String,
  name: String,
  transmittedFrom: String,
  description: String,
  type: String,
  processingStatus: Number,
  requiredUnits: Number,
  location: {
    creationDate: String,
    longtitude: String,
    latitude: String,
    adress: String,
    description: String
  },
  overtakingUnits: [ { type: Schema.ObjectId, ref: 'Unit' } ],
  arrivedUnits: [ { type: Schema.ObjectId, ref: 'Unit' } ]
}, { collection: 'emergencies' })

console.log('+++Emergency schema created')
export default mongoose.model('Emergency', emergencySchema)
