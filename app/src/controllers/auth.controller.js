import { mongoose } from '../connections/mongodb'
import Officer from '../models/officer.model'
import config from '../../config/db'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt-nodejs'
const env = process.env.NODE_ENV || 'development'

class authController {

  doAuthentication(username, password) {
    return this.getUser(username)
    .then((user) => {
      if(!user){
        return {auth: false, msg: 'Wrong username'}
      }
      const passwordHash = user.pw
      const passwordIsValid = bcrypt.compareSync(password, passwordHash)
      if (!passwordIsValid) {
        return {auth: false, msg: 'Wrong password'}
      } else if (passwordIsValid) {
        return this.getUserWithoutPassword(user._id)
        .then(officer => {
          const token = jwt.sign(officer, config[env].secret, {
           expiresIn: (60*60*8)
          })
          return {auth: true, msg: token, user: officer}
        })
      }
    })
    .catch((err) => console.log(err.message))
  }

  getUser(pseudo) {
    return Officer
    .findOne({pseudonym: pseudo})
    .then(officer => officer)
    .catch(err => err.message)
  }

  getUserWithoutPassword(id) {
    return Officer
    .findOne({ _id: id })
    .select('-pw')
    .then(officer => officer)
    .catch(err => err.message)
  }

}

export default authController
