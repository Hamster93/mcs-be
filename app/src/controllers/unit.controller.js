import { mongoose } from '../connections/mongodb'
import Unit from '../models/unit.model'

const UNIT_STATUS = {
  'available': 1,
  'on_the_way': 2,
  'employed': 3
}

class UnitController {

  createUnit(unit) {
    return new Unit({
      name: unit.name,
      type: unit.type,
      capacity: unit.capacity,
      equipment: unit.equipment,
      status: UNIT_STATUS.available,
      location: unit.location,
      officer: unit.officer
    })
    .save()
    .catch(err => err.message)
  }

  getUnit(id) {
    return Unit
    .findOne({ _id: id })
    .then(unit => unit)
    .catch(err => err.message)
  }

  getUnitByOfficer(officerId) {
    return Unit
    .findOne({ officer: officerId })
    .then(unit => unit)
    .catch(err => err.message)
  }

  updateUnit(unit) {
    return Unit
    .update({ _id: unit._id }, { $set: unit })
    .catch(err => err.message)
  }

  updateUnitStatus(unitId, unitStatus) {
    return Unit
    .update({ _id: unitId }, { $set: { status: parseInt(unitStatus) } })
    .catch(err => err.message)
  }

  getUnits() {
    return Unit
    .find({})
    .then(units => units)
    .catch(err => err.message)
  }

  employUnit(unit) {
    unit.status = UNIT_STATUS.on_the_way
    return this.updateUnit(unit)
  }

  unitArrived(unit) {
    unit.status = UNIT_STATUS.employed
    return this.updateUnit(unit)
  }

  unitEmergencyDone(unit){
    unit.status = UNIT_STATUS.available
    console.log('UNIT ' + unit + ' DONE');
    return this.updateUnit(unit)
  }
}
export default UnitController
