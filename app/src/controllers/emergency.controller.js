import { mongoose } from '../connections/mongodb'
import Emergency from '../models/emergency.model'
import UnitController from './unit.controller'
const unitController = new UnitController()

const PROCESSING_STATUS = {
  open: 1,
  demandEngaged: 2,
  progress: 3,
  done: 4
}

class EmergencyController {

  createEmergency (emergency) {
    return new Emergency({
      creationDate: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
      name: emergency.name,
      transmittedFrom: emergency.transmittedFrom,
      description: emergency.description,
      type: emergency.type,
      processingStatus: PROCESSING_STATUS.open,
      requiredUnits: emergency.requiredUnits,
      location: emergency.location,
      overtakingUnits: emergency.overtakingUnits,
      arrivedUnits: emergency.arrivedUnits
    })
    .save()
    .catch( err => err.message )
  }

  getEmergency (id) {
    return Emergency
    .findOne( { _id: id } )
    .then( emergency => emergency )
    .catch( err => err.message )
  }

  updateEmergency (emergency) {
    return Emergency
    .update({ _id: emergency._id }, { $set: emergency })
    .catch( err => err.message )
  }

  getEmergencies () {
    return Emergency
    .find( {} )
    .then( emergencies => emergencies )
    .catch( err => err.message )
  }

  emergencyUnitArrived(emergency, unit) {
      if(emergency.arrivedUnits.length < emergency.overtakingUnits.length){
        emergency.arrivedUnits.push(unit)
        emergency.processingStatus = PROCESSING_STATUS.progress
      }
      return this.updateEmergency(emergency)
  }

  overtakeEmergency (emergency, unit) {
    if (emergency.requiredUnits > 0) {
      emergency.overtakingUnits.push(unit)
      emergency.requiredUnits--
      if(emergency.requiredUnits == 0 && emergency.processingStatus < PROCESSING_STATUS.progress){
        emergency.processingStatus = PROCESSING_STATUS.demandEngaged
      }
    }
    return this.updateEmergency(emergency)
  }

  getOvertakenEmergency(unitId) {
    return new Promise((resolve, reject) => {

      this.getEmergencies()
      .then(emergencies => {
        let overtakenEmergency
          emergencies.forEach(emergency => {
            if(emergency.processingStatus < 4){
              emergency.overtakingUnits.forEach(unitID => {
                if(unitID == unitId){
                  unitController.getUnit(unitID)
                  .then((unit) => {
                    console.log('unit: ' + unit.name)
                    console.log('unit status: ' + unit.status)
                    if(unit.status > 1){
                        console.log('unit status > 1: ' + unit.status)
                        console.log('emergency HIT:' + emergency)
                        resolve(emergency)
                    }
                  })
                }
              })
            }
          })
      })
    })
  }

  emergencyDone(emergency, unit) {
    let isStatusOfAllUnitsAvailable
    let doneUnits = []
    return new Promise((resolve, reject) => {
      emergency.overtakingUnits.forEach(unitId => {
        let unitIsAvailable
        unitController.getUnit(unitId)
        .then((unit) => {
          if (unit.status === 1) {
            doneUnits.push(unit)
          }
          console.log('units done: ' + doneUnits)
          if (doneUnits.length === emergency.overtakingUnits.length) {
            console.log('ALL UNITS DONE');
            emergency.processingStatus = PROCESSING_STATUS.done
            this.updateEmergency(emergency)
            .then(() => {
              resolve(this.getEmergency(emergency._id))
            })
          }
        })
      })
    })

  }
}

export default EmergencyController
