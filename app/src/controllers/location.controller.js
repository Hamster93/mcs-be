import { mongoose } from '../connections/mongodb'
import Location from '../models/location.model'

class LocationController {

  createLocation (location) {
    return new Location({
      creationDate: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
      longtitude: location.longtitude,
      latitude: location.latitude,
      adress: location.adress
    })
    .save()
    .catch( err => err.message )
  }

  getLocation (id) {
    return Location
    .findOne( { _id: id } )
    .then( location => location )
    .catch( err => err.message )
  }

  updateLocation (location) {
    return Location
    .update({ _id: location._id }, { $set: location })
    .catch( err => err.message )
  }

  getLocations () {
    return Location
    .find( {} )
    .then( locations => locations )
    .catch( err => err.message )
  }

}

export default LocationController
