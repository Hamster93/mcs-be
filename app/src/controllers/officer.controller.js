import { mongoose } from '../connections/mongodb'
import Officer from '../models/officer.model'

class OfficerController {

  createOfficer (officer) {
    return new Officer({
      name: officer.name,
      pseudonym: officer.pseudonym,
      pw: officer.pw,
      status: officer.status,
      unit: officer.unit
    })
    .save()
    .catch( err => err.message )
  }

  getOfficer (id) {
    return Officer
    .findOne( { _id: id } )
    .select( '-pw' )
    .then( officer => officer )
    .catch( err => err.message )
  }

  updateOfficer (officer) {
    return Officer
    .update({ _id: officer._id }, { $set: officer })
    .catch( err => err.message )
  }

  getOfficers () {
    return Officer
    .find( {} )
    .select( '-pw' )
    .then( officers => officers )
    .catch( err => err.message )
  }

}

export default OfficerController
