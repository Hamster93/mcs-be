import socket from 'socket.io'
import EmergencyController from '../controllers/emergency.controller'
import UnitController from '../controllers/unit.controller'
const emergencyController = new EmergencyController()
const unitController = new UnitController()

export default (server) => {
  const io = new socket(server)
  io.on('connection', connection => {
    console.log('SOCKET CONNECTED ' + connection.id)

    connection.on('new_emergency', emergency => {
      const emergencyJson = JSON.parse(emergency)
      emergencyController.createEmergency(emergencyJson)
      .then(emergencyJson => io.sockets.emit('emergency_create', emergencyJson))
      .catch(console.log)
    })

    connection.on('overtake_emergency', (emergency, unit) => {
      const emergencyJson = JSON.parse(emergency)
      const unitJson = JSON.parse(unit)
      emergencyController.overtakeEmergency(emergencyJson, unitJson)
      .then(() => {
        unitController.employUnit(unitJson)
        .then(() => {
          emergencyController.getEmergency(emergencyJson._id)
          .then((updatedEmergency) => io.sockets.emit('overtake_update_emergency', updatedEmergency))
          unitController.getUnit(unitJson._id)
          .then(updatedUnit => io.sockets.emit('overtake_update_unit', updatedUnit))
        })
      })
      .catch(console.log)
    })

    connection.on('new_unit_location', unit => {
      const unitJson = JSON.parse(unit)
      unitController.updateUnit(unitJson)
      .then(() => {
        unitController.getUnit(unitJson._id)
        .then((updatedUnit) => io.sockets.emit('unit_location_update', updatedUnit))
        .catch(console.log)
      })
      .catch(console.log)
    })

    connection.on('unit_arrived', unit => {
      const unitJson = JSON.parse(unit)
      unitController.unitArrived(unitJson)
      .then(() => {
        unitController.getUnit(unitJson._id)
        .then((updatedUnit) => io.sockets.emit('unit_arrived_update', updatedUnit))
        .catch(console.log)
      })
      .catch(console.log)
    })

    connection.on('unit_arrived_emergency', (emergency, unit) => {
      const unitJson = JSON.parse(unit)
      const emergencyJson = JSON.parse(emergency)
      emergencyController.emergencyUnitArrived(emergencyJson, unitJson)
      .then(() => {
        emergencyController.getEmergency(emergencyJson._id)
        .then(updatedEmergency => io.sockets.emit('unit_arrived_emergency_update', updatedEmergency))
        .catch(console.log)
      })
      .catch(console.log)
    })

    connection.on('unit_done_emergency', (unit, emergency) => {
      console.log('UNIT: ' + unit + ' done emergency ' + emergency);
      const unitJson = JSON.parse(unit)
      const emergencyJson = JSON.parse(emergency)
      unitController.unitEmergencyDone(unitJson)
      .then(() => {
        unitController.getUnit(unitJson._id)
        .then((updatedUnit) => io.sockets.emit('unit_emergency_done_update', updatedUnit))
        emergencyController.emergencyDone(emergencyJson, unitJson)
        .then((updatedEmergency) => io.sockets.emit('emergency_done_update', updatedEmergency))
        .catch(console.log)
      })
      .catch(console.log)
    })

  })
}
