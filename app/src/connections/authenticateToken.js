import jwt from 'jsonwebtoken'
const config = require('../../config/db');
const env = process.env.NODE_ENV || 'development'

class TokenCheck {
  verifyToken (token) {
      if (!token) {
        return false
      } else{
        try{
          var decoded = jwt.verify(token, config[env].secret)
          return true
        } catch (err) {
          return false
        }
      }
  }
}
export default TokenCheck
