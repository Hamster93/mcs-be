import mongoose from 'mongoose'
import db from '../../config/db'
import bluebird from 'bluebird';
const env = process.env.NODE_ENV || 'development'

mongoose.Promise = bluebird;

mongoose.openConnection = () => {
  const options = {
    useMongoClient: true,
    promiseLibrary: global.Promise,
    keepAlive: 120
  }
  mongoose.connect(
    db[env].databaseUrl,
    options,
    () => { console.log('+++Connected to mongoose') }
  )
}

mongoose.closeConnection = () => {
  mongoose.disconnect(
    () => { console.log('---Disconnected from Mogoose') }
  )
}

export { mongoose }
