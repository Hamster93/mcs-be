var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
import AuthController from '../src/controllers/auth.controller'
const authController = new AuthController()
var config = require('./db');
const env = process.env.NODE_ENV || 'development'

export default (passport) => {
  var opts = {}
  opts.jwtFromRequest = ExtractJwt.fromHeader('Authorization')
  opts.secretOrKey = config[env].secret
  passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
    console.log('ENTERED PASSPORT');
      authController.getUserById(jwt_payload._doc._id)
      .then( user => {
        console.log(user);
        if(err)
          done(err, false)
        if(user)
          done(null, user)
      })
      .catch( err => {
        console.log("PASSPORT ERROR");
        done(err, false)} )
    })
  )
}
