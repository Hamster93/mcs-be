export default {
  'development' : {
    'secret': 'someheavysecrettosetheresomethingtopsecretstuff',
    'databaseUrl': 'mongodb://mongo:27017'
  },
  'production': {
      'secret': ',:"=;"§_$"!$?)$%/(&%=)($?!heavysecrettosethere90335785349123345%"$§=)/(§?%="($',
      'databaseUrl': 'mongodb://acs.in.htwg-konstanz.de:32769'
  },
  'test': {
      'secret': 'someheavysecrettotestsomethingblablblabla'
  }
}
