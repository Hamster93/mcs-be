import mockingoose from 'mockingoose'
import authController from '../src/controllers/auth.controller'
import bcrypt from 'bcrypt-nodejs'
import config from '../config/db'
import jwt from 'jsonwebtoken'

const AuthController = new authController()

test('authentication fails when wrong username was passed', () => {
    mockingoose.Officer.toReturn(null, 'findOne')
    AuthController.doAuthentication('name1', '12@ada3!asdg')
    .then(msg => {
        expect(msg).toEqual({auth: false, msg: 'Wrong username'})
    })
})

test('authentication fails when wrong password was passed', () => {
    mockingoose.Officer.toReturn({pw: '$2a$10$JryT/6PNf.2yJjRbhfIfuO3o1WhFXd6Y7OyV04RejioxWMGY86w6W'}, 'findOne')
    AuthController.doAuthentication('name1', 'root1')
    .then(msg => {
        expect(msg).toEqual({auth: false, msg: 'Wrong password'})
    })
})

test('authentication generates jwt token when credentials are right', () => {
    mockingoose.Officer.toReturn({pw: '$2a$10$JryT/6PNf.2yJjRbhfIfuO3o1WhFXd6Y7OyV04RejioxWMGY86w6W'}, 'findOne')
    AuthController.doAuthentication('name1', 'root')
    .then(msg => {
        expect(jwt.decode(msg.msg)._doc.pw).toBe('$2a$10$JryT/6PNf.2yJjRbhfIfuO3o1WhFXd6Y7OyV04RejioxWMGY86w6W')
    })
})
