import http from 'http'
import express from 'express'
import bodyParser from 'body-parser'
import { mongoose } from './src/connections/mongodb'
import { buildSchema } from 'graphql'
import routes from './src/routes/mcs.routes'
import socket from './src/sockets/mcs.socket.js'
import auth from './config/auth'

const env = process.env.NODE_ENV || 'development'
const port = process.env.PORT || 3000
const hostname = '127.0.0.1'
const app = express()
const server = http.Server(app)

app
.use(bodyParser.urlencoded({ extended: false }))
.use(bodyParser.json())

mongoose.openConnection()

mongoose.connection.on('connected', function () {
  console.log('Mongoose connection open')
});

mongoose.connection.on('error',function (err) {
  console.log('Mongoose connection error: ' + err)
});

mongoose.connection.on('disconnected', function () {
  console.log('Mongoose connection disconnected')
});

process.on('SIGINT', function() {
  mongoose.closeConnection()
});


routes(app)
socket(server)

server.listen(port, () => { console.log('+++Express Server is running on port ' +  port) })
